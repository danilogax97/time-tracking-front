import React, { Component } from 'react';
import { replace } from 'connected-react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Api from '../lib/api';
import { BaseWrapper, PageWrapper } from '../components/atoms/Wrappers';
import Header from '../components/moleculs/Header';
import { WelcomeMessageText } from '../components/WelcomeMessageDashboard';
import { signOut } from '../redux/sign-in/SignInActions';
import { setError } from '../redux/error/ErrorActions';

class SignOut extends Component {
	async componentDidMount() {
		const {
			token,
			actions,
		} = this.props;
		try {
			await Api.signOut(token);
			actions.signOut();
		} catch (err) {
			actions.setError(err);
		}
	}

	render() {
		return (
			<BaseWrapper>
				<Header />
				<PageWrapper>
					<WelcomeMessageText>
					See you soon
					</WelcomeMessageText>
				</PageWrapper>
			</BaseWrapper>
		);
	}
}
const mapStateToProps = (state) => ({
	token: state.getIn(['signInReducer', 'user', 'token']),
});
const mapDispatchToProps = (dispatch) => ({
	actions: bindActionCreators({
		replace,
		signOut,
		setError,
	}, dispatch),
});
export default connect(mapStateToProps, mapDispatchToProps)(SignOut);
