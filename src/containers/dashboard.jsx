import React, { Component } from 'react';
import { connect } from 'react-redux';
import dayjs from 'dayjs';
import { LoggedInHeader } from '../components/atoms/LoggedInHeader';
import { WelcomeMessageDashboard } from '../components/WelcomeMessageDashboard';
import { LastSessionBox } from '../components/LastSessionBox';
import Timer from '../components/Timer';
import { PageWrapper } from '../components/atoms/Wrappers';
import { FlexColumn } from '../components/atoms/Flex';
import Api from '../lib/api';
import { setError } from '../redux/error/ErrorActions';

class Dashboard extends Component {
	constructor(props) {
		super(props);
		this.state = undefined;
	}

	async componentDidMount() {
		const {
			token,
			setError,
		} = this.props;
		try {
			const response = await Api.getLastActivity(token);
			if (response.data.report) {
				const {
					startTime,
					endTime,
				} = response.data.report;
				this.setState({ startTime: dayjs(startTime), endTime: dayjs(endTime) });
			}
		} catch (err) {
			console.log(err);
			setError(err);
		}
	}

	render() {
		const {
			username,
			running,
		} = this.props;

		return (
			<>
				<PageWrapper>
					<LoggedInHeader />
					<FlexColumn>
						<WelcomeMessageDashboard
							username={username}
							on={running}
						/>
						<LastSessionBox date={this.state} />
						<Timer />
					</FlexColumn>
				</PageWrapper>
			</>
		);
	}
}

const mapStateToProps = (state) => ({
	username: state.getIn(['signInReducer', 'user', 'username']),
	email: state.getIn(['signInReducer', 'user', 'email']),
	running: state.getIn(['timerReducer', 'running']),
	token: state.getIn(['signInReducer', 'user', 'token']),
});
export default connect(mapStateToProps, { setError })(Dashboard);
