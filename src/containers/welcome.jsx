import React from 'react';
import { BaseWrapper, PageWrapper } from '../components/atoms/Wrappers';
import Header from '../components/moleculs/Header';
import { HomePageLink } from '../components/atoms/Links';
import { FlexColumn } from '../components/atoms/Flex';
import withSwitch from '../components/hoc/switch';

const Welcome = {
	LoggedOut: () => (
		<BaseWrapper>
			<Header />
			<PageWrapper>
				<FlexColumn>
					<HomePageLink to="/sign-up">
							Sign up
					</HomePageLink>
					<HomePageLink to="/sign-in">
							Sign in
					</HomePageLink>
				</FlexColumn>
			</PageWrapper>
		</BaseWrapper>
	),
	LoggedIn: () => (
		<BaseWrapper>
			<Header />
			<PageWrapper>
				<FlexColumn>
					<HomePageLink to="/dashboard">
								Dashboard
					</HomePageLink>
					<HomePageLink to="/reports">
								Reports
					</HomePageLink>
				</FlexColumn>
			</PageWrapper>
		</BaseWrapper>
	),

};
export default withSwitch(Welcome);
