import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import LoggedInHeaderWithBackButton from '../components/atoms/LoggedInHeader';
import { PageWrapper } from '../components/atoms/Wrappers';
import { WelcomeMessageText, WelcomeMessageWrapper } from '../components/WelcomeMessageDashboard';
import { Report } from '../components/report';
import { fetchReports } from '../redux/reports/ReportsActions';


class Reports extends Component {
	async componentDidMount() {
		const {
			actions,
			token,
		} = this.props;
		await actions.fetchReports(token);
	}

	render() {
		const {
			reports,
			loading,
		} = this.props;

		return loading ? <div>LOADING</div> : (

			<PageWrapper>
				<LoggedInHeaderWithBackButton />
				<WelcomeMessageWrapper>
					<WelcomeMessageText>
						Hey, it's your daily report
					</WelcomeMessageText>

					{Object.keys(reports).map((el) => (
						<Report
							day={el}
							activities={reports[el].reports}
							total={reports[el].total}
						/>
					))}
				</WelcomeMessageWrapper>

			</PageWrapper>

		);
	}
}
const mapStateToProps = (state) => ({
	reports: state.getIn(['reportsReducer', 'reports']).toJS(),
	loading: state.getIn(['reportsReducer', 'loading']),
	token: state.getIn(['signInReducer', 'user', 'token']),
});
const mapDispatchToProps = (dispatch) => ({
	actions: bindActionCreators({
		fetchReports,
	}, dispatch),
});
export default connect(mapStateToProps, mapDispatchToProps)(Reports);
