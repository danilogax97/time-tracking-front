import React, { useState } from 'react';
import { connect } from 'react-redux';
import useForm from 'react-hook-form';
import { push } from 'connected-react-router';
import Header from '../components/moleculs/Header';
import { ErrorField } from '../components/atoms/Errors';
import {
	Heading,
} from '../components/atoms/Header';
import {
	BaseForm, EmailInput, PasswordInput, SubmitButton, UsernameInput,
} from '../components/atoms/Input';

import Api from '../lib/api';
import { FlexColumn } from '../components/atoms/Flex';
import Unprotected from '../components/hoc/unprotected';

const SignUp = ({ push }) => {
	const {
		register, handleSubmit, reset, errors, setError,
	} = useForm();
	const [loading, setLoading] = useState(false);


	const onSubmit = async (data) => {
		reset();
		setLoading(true);
		try {
			await Api.signUp(data);
			setLoading(false);
			push('/sign-in');
		} catch (err) {
			setLoading(false);
			if (err.response.status === 409) {
				setError('email', 'in use', 'Seems like you already have an account');
			} else {
				setError(err);
			}
		}
	};

	return (loading ? <div>LOADING</div> : (
		<>
			<Header />
			<FlexColumn>

				<Heading text="Sign Up" />

				<BaseForm
					onSubmit={handleSubmit(onSubmit)}
				>

					<UsernameInput
						type="text"
						name="fullName"
						placeholder="username"
						ref={register({ required: 'Username is required' })}
					/>
					{errors.fullName && <ErrorField message={errors.fullName.message} />}

					<EmailInput
						type="text"
						name="email"
						placeholder="email"
						ref={register({
							required: 'Email is required',
							pattern: {
								value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
								message: 'Invalid email format',
							},
						})}
					/>
					{errors.email && <ErrorField message={errors.email.message} />}

					<PasswordInput
						type="password"
						name="password"
						placeholder="password"
						ref={register({ required: 'Password is required' })}
					/>
					{errors.password && <ErrorField message={errors.password.message} />}

					<SubmitButton
						type="submit"
						value="Join"
					/>
				</BaseForm>

			</FlexColumn>
		</>
	));
};

export default connect(null, { push })(Unprotected(SignUp));
