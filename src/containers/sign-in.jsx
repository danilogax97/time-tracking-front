import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Header from '../components/moleculs/Header';
import {
	Heading,
} from '../components/atoms/Header';
import { fetchSignIn } from '../redux/sign-in/SignInActions';
import { BaseWrapper } from '../components/atoms/Wrappers';
import {
	BaseForm, EmailInput, PasswordInput, SubmitButton,
} from '../components/atoms/Input';
import { FlexColumn } from '../components/atoms/Flex';
import { ErrorField } from '../components/atoms/Errors';
import Unprotected from '../components/hoc/unprotected';

class SignIn extends Component {
	handleSubmit(event) {
		event.preventDefault();

		const {
			email,
			password,
		} = event.target;

		const {
			actions,
		} = this.props;

		actions.fetchSignIn({
			username: email.value,
			password: password.value,
		});
	}

	render() {
		const {
			error,
		} = this.props;
		return (
			<BaseWrapper>
				<Header />
				<FlexColumn>

					<Heading text="Sign In" />

					<BaseForm onSubmit={(event) => this.handleSubmit(event)}>

						{error && <ErrorField message="Email or password are incorrect please try again" />}

						<EmailInput
							type="text"
							name="email"
							placeholder="email"
						/>

						<PasswordInput
							type="password"
							name="password"
							placeholder="password"
						/>
						<SubmitButton
							type="submit"
							value="Sign in"
						/>
					</BaseForm>
				</FlexColumn>
			</BaseWrapper>
		);
	}
}
const mapStateToProps = (state) => ({
	error: state.getIn(['signInReducer', 'error']),
});
const mapDispatchToProps = (dispatch) => ({
	actions: bindActionCreators({
		fetchSignIn,
	}, dispatch),
});
export default connect(mapStateToProps, mapDispatchToProps)(Unprotected(SignIn));
