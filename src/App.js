import React from 'react';
import './assets/css/reset.css';
import { Provider } from 'react-redux';
import MainRouter from './MainRouter';
import configureStore, { history } from './redux/redux-store';

export const store = configureStore();
if (localStorage.getItem('TTtoken')) {
	store.dispatch({
		type: 'ADD_TOKEN_FROM_PERSISTANCE',
		token: localStorage.getItem('TTtoken'),
		username: localStorage.getItem('TTusername'),
	});
}

const App = () => (
	<Provider store={store}>
		<MainRouter history={history} />
	</Provider>
);

export default App;
