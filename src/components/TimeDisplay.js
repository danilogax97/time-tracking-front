import React from 'react';
import styled from 'styled-components';
import { FlexRowSpacedBetween } from './atoms/Flex';
import { Color } from './atoms/Constants';

const TimeDisplayFlexBox = styled(FlexRowSpacedBetween)`
  margin-top: 50px;
  margin-bottom: 50px;
`;
export const Time = styled.div`
 font-size: 62px;
 position: relative;
 ${Color.darkBlue};
`;
const TimeTextLightGray = styled.span`
  position: absolute;
  top: 65px;
  right: 10px;
  ${Color.gray};
  font-size: 14px;
`;
export const TimeDisplay = ({ time }) => (
	<TimeDisplayFlexBox>
		<Time>
			{time.format('HH') }
			<TimeTextLightGray>
                Hours
			</TimeTextLightGray>
		</Time>
		<Time>
			{time.format('mm') }
			<TimeTextLightGray>
                Minutes
			</TimeTextLightGray>
		</Time>
		<Time>
			{time.format('ss') }
			<TimeTextLightGray>
                Seconds
			</TimeTextLightGray>
		</Time>
	</TimeDisplayFlexBox>
);
