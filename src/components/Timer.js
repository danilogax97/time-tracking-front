import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import dayjs from 'dayjs';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { replace } from 'connected-react-router';
import { Heading } from './atoms/Header';
import { TimeDisplay } from './TimeDisplay';
import { startTimer, stopTimer, tick } from '../redux/timer/TimerActions';
import Api from '../lib/api';
import { Color } from './atoms/Constants';
import { signOut } from '../redux/sign-in/SignInActions';
import { setError } from '../redux/error/ErrorActions';
import { TimerButton, TimerWrapper } from './atoms/Timer';

const DailyReportNavLink = styled(Link)`
	text-decoration: underline;
	font-weight: 400;
	${Color.darkBlue};
`;
class Timer extends PureComponent {
	async handleClick() {
		const {
			actions,
			running,
			startTime,
			token,
		} = this.props;

		if (!running) {
			const interval = setInterval(() => {
				actions.tick();
			}, 1000);
			actions.startTimer(interval);
		} else {
			const stopTime = dayjs();
			actions.stopTimer();

			// maybe move this to actions???
			try {
				await Api.postActivity(token,
					// startTime.format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
					// stopTime.format('YYYY-MM-DDTHH:mm:ss.SSSZ'));
					startTime,
					stopTime);
			} catch (err) {
				actions.setError(err);
			}
		}
	}

	render() {
		const {
			currentTime,
			running,
		} = this.props;
		return (

			<TimerWrapper>
				<Heading text="Current session" />

				<TimeDisplay time={currentTime} />

				<TimerButton
					type="submit"
					active={running}
					value={running ? 'Stop timer' : 'Start'}
					onClick={() => this.handleClick()}
				/>

				<DailyReportNavLink to="/reports">
						See your daily report
				</DailyReportNavLink>

			</TimerWrapper>

		);
	}
}
const mapStateToProps = (state) => ({
	running: state.get('timerReducer').get('running'),
	currentTime: state.get('timerReducer').get('currentTime'),
	startTime: state.get('timerReducer').get('startTime'),
	token: state.getIn(['signInReducer', 'user', 'token']),
});
const mapDispatchToProps = (dispatch) => (
	{
		actions: bindActionCreators({
			startTimer,
			stopTimer,
			tick,
			replace,
			setError,
			signOutFromStore: signOut,
		}, dispatch),
	}
);
export default connect(mapStateToProps, mapDispatchToProps)(Timer);
