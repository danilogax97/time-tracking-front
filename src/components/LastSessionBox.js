import styled from 'styled-components';
import React from 'react';
import { BackgroundColor } from './atoms/Constants';
import { displayDifference } from '../lib/helpers';

const LastSessionBoxWrapper = styled.div`
  margin-top: 28px;
  width: 100%;
  height: 100px;
  ${BackgroundColor.lightGray}
  font-size: 2vh;
`;
const FloatLeftBox = styled.div`
  height: 100%;
  width: 60%;
  float:left;
  display: flex;
  justify-content: center;
  align-items: center;
`;
const FloatRightBox = styled.div`
  height: 100%;
  width: 40%;
  float: right;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  ${BackgroundColor.darkerGray};
`;
const TextBold = styled.div`
  font-weight: 500;
  line-height: 1.5;
`;
const TextNormal = styled.div`
  line-height: 1.5;
  font-weight: 400;
`;
const NoSessionBox = styled(FloatLeftBox)`
  ${BackgroundColor.lightGray};
  font-weight: 500;
  line-height: 1.5;
  width: 100%;
  height: 100px;
  margin-top: 20px;
`;

export const LastSessionBox = ({ date }) => (
	date ?
		(
			<LastSessionBoxWrapper>
				<FloatLeftBox>
					<TextBold>
                Last session
					</TextBold>
				</FloatLeftBox>
				<FloatRightBox>
					<TextBold>
						{displayDifference(date.startTime, date.endTime)}
					</TextBold>
					<TextNormal>
						{date.startTime.format('DD[th] MMMM')}
					</TextNormal>
				</FloatRightBox>
			</LastSessionBoxWrapper>
		) : (
			<NoSessionBox>
			Go ahead and start your first session
			</NoSessionBox>
		)
);
