import React from 'react';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { signOut } from '../redux/sign-in/SignInActions';

class ErrorBoundary extends React.Component {
	constructor(props) {
		super(props);
		this.state = { hasError: false };
	}

	static getDerivedStateFromError(error) {
		return { hasError: true };
	}

	componentDidCatch(error, info) {
	}

	render() {
		const {
			error,
			signOut,
			reportsFetchError,
		} = this.props;
		// handle error appropriately
		if (error || reportsFetchError || this.state.hasError) {
			if (error && error.message === 'Network Error') {
				alert("Can't connect to the server");
			} else {
				alert('something went wrong');
				signOut();
			}
		}

		return this.props.children;
	}
}
const mapStateToProps = (state) => ({
	error: state.get('errorReducer').get('error'),
	reportsFetchError: state.get('reportsReducer').get('error'),
});
export default connect(mapStateToProps, { signOut, push })(ErrorBoundary);
