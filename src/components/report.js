import React from 'react';
import styled from 'styled-components';
import dayjs from 'dayjs';
import advanced from 'dayjs/plugin/advancedFormat';
import { LightBlueText, SlightlyBoldBlueText } from './WelcomeMessageDashboard';
import { ReportWrapper } from './atoms/Wrappers';
import { ReportHeader } from './atoms/Header';
import { displayDifference, figureOutIfItsTodayOrYesterday } from '../lib/helpers';

dayjs.extend(advanced);

const ReportDisplay = styled.div`
	background-color: #dcdcdc;
	margin-top: 5px;
	line-height: 1.4;
	font-size: 14px;
	display: flex;
	justify-content: space-between;
	padding: 5px;
`;
export const Header = ({ day, extra }) => (
	<SlightlyBoldBlueText>
		{extra}
		{' '}
		{' '}
		{day}
	</SlightlyBoldBlueText>
);
export const Report = ({ day, activities, total }) => {
	if (!activities || !activities[0]) {
		return null;
	}

	return (
		<ReportWrapper>
			<ReportHeader>
				{/* go with null if its not today or yesterday */}
				<Header
					day={day}
					extra={figureOutIfItsTodayOrYesterday(activities[0].startTime)}
				/>
				<LightBlueText>
					{total.format('HH:mm:ss')}
				</LightBlueText>
			</ReportHeader>
			{activities.map((el) => (
				<ReportDisplay>
					<LightBlueText>
						{dayjs(el.startTime, 'YYYY-MM-DDTHH:mm:ss.SSSZ').format('HH:mm:ss -')}
						{' '}
						{dayjs(el.endTime, 'YYYY-MM-DDTHH:mm:ss.SSSZ').format('HH:mm:ss')}
					</LightBlueText>
					<LightBlueText>{displayDifference(el.startTime, el.endTime)}</LightBlueText>
				</ReportDisplay>
			))}
		</ReportWrapper>
	);
};
