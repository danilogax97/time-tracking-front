import styled from 'styled-components';
import React from 'react';
import { Color } from './Constants';

export const ErrorMessage = styled.span`
  ${Color.errorColor};
  font-size: 14px;
`;
export const ErrorMessageWrapper = styled.div`
	padding-top: 5px;
`;

export const ErrorField = ({ message }) => (
	<ErrorMessageWrapper>
		<ErrorMessage>
			{message}
		</ErrorMessage>
	</ErrorMessageWrapper>
);
