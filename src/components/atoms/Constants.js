import { css } from 'styled-components';

export const Color = {
	lightPurple: css`
		color: #af79ff;
	`,
	purple: css`
      color: blueviolet;
    `,
	lightGreen: css`
      color: #33ca9d;
    `,
	gray: css`
      color: gray;
    `,
	white: css`
      color: white;
    `,
	errorColor: css`
		color: #ff4a27;
	`,
	darkBlue: css`
		color: midnightblue
	`,
};

export const BackgroundColor = {
	darkBlue: css`
		background-color: #173554;
	`,
	lightGray: css`
		background-color: whitesmoke;
	`,
	darkerGray: css`
		background-color: #e6e6e6;
	`,
	lightGreen: css`
      background-color: #33ca9d;
    `,
	purple: css`
      background-color: blueviolet;
    `,
};
