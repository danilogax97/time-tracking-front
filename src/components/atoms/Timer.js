import styled, {css, keyframes} from "styled-components";
import {BackgroundColor, Color} from "./Constants";
import {SubmitButton} from "./Input";

const tickingKeyFrames = keyframes`
	0%{
		color: white;
	}
	100%{
		color: transparent;
	}
`;
export const TimerWrapper = styled.div`
  margin-top: 45px;
  text-align: center;
`;
const TimerButtonActive = css`
	animation-name: ${tickingKeyFrames};
    animation-duration: 1.6s;
    animation-timing-function: linear;
    animation-iteration-count: infinite;
    animation-direction: alternate;
    animation-play-state: running;
    ${BackgroundColor.purple};
    ${Color.white};
`;
export const TimerButton = styled(SubmitButton)`
	${BackgroundColor.lightGreen};
	${Color.white};
	text-align: center;
	margin-bottom: 20px;
	${(props) => props.active && TimerButtonActive};
`;
