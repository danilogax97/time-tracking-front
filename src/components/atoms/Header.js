import styled from 'styled-components';
import React from 'react';
import { BackgroundColor, Color } from './Constants';
import { FlexRowSpacedBetween } from './Flex';

export const HeaderWrapper = styled.div`
  width: 100%;
  height: 100px;
  ${BackgroundColor.darkBlue};
  padding-top: 15px;
  padding-left: 15px;
  -webkit-border-bottom-right-radius: 100%;
  margin-bottom: 25px;
`;
export const LogoTextGray = styled.span`
  display: block;
  font-size: 20px;
  ${Color.gray};
`;
export const LogoTextGreen = styled.span`
  display: block;
  font-size:30px;
  font-weight: bold;
  ${Color.lightGreen}
`;
export const HeadingText = styled.h1`
  ${Color.gray};
  font-size: 28px;
  font-weight: bold;
`;
export const HeadingTextWrapper = styled.div`
  padding: 20px;
`;
export const ReportHeader = styled(FlexRowSpacedBetween)`
  margin-top: 20px;
  margin-bottom: 10px;
`;
export const Heading = ({ text }) => (
	<HeadingTextWrapper>
		<HeadingText>
			{text}
		</HeadingText>
	</HeadingTextWrapper>
);
