import styled from 'styled-components';

export const BaseFlex = styled.div`
	display: flex;
`;
export const FlexRow = styled(BaseFlex)`
	align-items: center;
	justify-content: center;
`;
export const FlexColumn = styled(FlexRow)`
	flex-direction: column;
`;
export const FlexRowSpacedBetween = styled(FlexRow)`
	justify-content: space-between;
`;
export const FlexRowSpaceAround = styled(FlexRow)`
	justify-content: space-around;
`;

