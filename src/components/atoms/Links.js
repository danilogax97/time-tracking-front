import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { Color } from './Constants';

export const BaseLinkWrapper = styled(Link)`
  text-decoration: none;
`;
export const PurpleSignOutLink = styled(BaseLinkWrapper)`
  ${Color.purple};
  text-decoration: underline;
  position: absolute;
  font-size: 13px;
  right: 0px;
  top: 17px;
`;
export const GreenLink = styled(BaseLinkWrapper)`
  ${Color.lightGreen};
  text-decoration: underline;
  font-size: 30px;
  display: block;
`;
export const HomePageLink = styled(GreenLink)`
  padding:20px;
  font-size: 40px;
`;
