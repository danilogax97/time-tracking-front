import styled from 'styled-components';
import React from 'react';
import { connect } from 'react-redux';
import { goBack } from 'connected-react-router';
import { FlexRow } from './Flex';
import { Color } from './Constants';
import LeftArrow from '../../assets/left-arrow.svg';
import { PurpleSignOutLink } from './Links';

const GrayLogoText = styled.span`
  font-size: 18px;
  ${Color.gray};
  font-weight: 500;
`;
const GreenLogoText = styled.span`
  font-size: 28px;
  ${Color.lightGreen};
  font-weight: 700;
  margin-left: 2px;
`;
const LogoTextWrapper = styled.div`
	display: inline;
	margin-top: 8px;
`;
const BackButton = styled.img`
	width: 40px;
	height: 20px;
	position: absolute;
	left: 0px;
	top: 16px;
	cursor: pointer;
`;
const LogoText = () => (
	<>
		<GrayLogoText>
            time
		</GrayLogoText>
		<GreenLogoText>
            tracker
		</GreenLogoText>
	</>
);

export const LoggedInHeader = () => (

	<>
		<FlexRow>
			<LogoTextWrapper>
				<LogoText />
			</LogoTextWrapper>
			<PurpleSignOutLink to="/sign-out">
				Sign-out
			</PurpleSignOutLink>
		</FlexRow>
	</>
);
const LoggedInHeaderWithBackButton = ({ goBack }) => (
	<>
		<FlexRow>
			<BackButton
				src={LeftArrow}
				onClick={() => goBack()}
			/>
			<LogoTextWrapper>
				<LogoText />
			</LogoTextWrapper>
			<PurpleSignOutLink to="/sign-out">
				Sign-out
			</PurpleSignOutLink>
		</FlexRow>
	</>
);
export default connect(null, { goBack })(LoggedInHeaderWithBackButton);
