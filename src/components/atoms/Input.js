import styled from 'styled-components';
import { BackgroundColor, Color } from './Constants';


export const BaseInput = styled.input`
  appearance: none;
  display: block;
`;
export const BaseForm = styled.form`
  padding:20px;
`;
export const UserInput = styled(BaseInput)`
  border: 0;
  background: transparent;
  height: 25px;
  width: 300px;
  margin-top: 20px;
  :focus{
   ${Color.lightPurple};
  }
  :focus::placeholder{
       color:transparent;
  }
  ${Color.gray};
  border-bottom: solid 3px;
`;
export const BaseButton = styled(BaseInput)`
  border: 0;
  background: transparent;
`;
export const SubmitButton = styled(BaseButton)`
  margin-top: 36px;
  ${BackgroundColor.darkBlue};
  ${Color.white};
  height: 35px;
  width: 300px;
  box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.6);
  font-size: 17px;
  cursor: pointer;
  :focus{
   ${Color.white};
  }
`;
export const EmailInput = styled(UserInput)`
  
`;
export const PasswordInput = styled(UserInput)`
  
`;
export const UsernameInput = styled(UserInput)`
  
`;
