import styled from 'styled-components';

export const BaseWrapper = styled.div`
	max-width: 100%;
	position: relative;
`;
export const LogoWrapper = styled.div`
  margin-left: 15px;
`;
export const PageWrapper = styled(BaseWrapper)`
  margin-left: 10%;
  margin-right: 10%;
  @media (min-width: 1000px) {
    margin-left: 25%;
    margin-right: 25%;
  }
`;
export const ReportWrapper = styled(BaseWrapper)`

`;
