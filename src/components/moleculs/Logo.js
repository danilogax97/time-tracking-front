import React from 'react';
import { LogoWrapper } from '../atoms/Wrappers';
import { LogoTextGray, LogoTextGreen } from '../atoms/Header';

export const Logo = () => (
	<LogoWrapper>
		<LogoTextGray>
            time
		</LogoTextGray>
		<LogoTextGreen>
            tracker
		</LogoTextGreen>
	</LogoWrapper>
);
