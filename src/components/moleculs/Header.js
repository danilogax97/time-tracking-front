import React from 'react';
import { HeaderWrapper } from '../atoms/Header';
import { Logo } from './Logo';

const Header = () => (
	<HeaderWrapper>
		<Logo />
	</HeaderWrapper>
);

export default Header;
