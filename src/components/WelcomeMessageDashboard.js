import styled, { css } from 'styled-components';
import React from 'react';
import dayjs from 'dayjs';
import { Color } from './atoms/Constants';

const getCurrentDay = () => (
	dayjs().format('dddd')
);
export const WelcomeMessageWrapper = styled.div`
 margin-top: 45px;
 width: 100%;
`;
const BoldBlueText = styled.span`
display: block;
margin-top: 6px;
font-weight: 500;
${Color.darkBlue};
`;
export const WelcomeMessageText = styled(BoldBlueText)`
  font-size: 30px;
`;
const ActiveTimer = css`
	${Color.lightGreen};
`;
const NormalPurpleText = styled(BoldBlueText)`
	font-size: 16px;
	${Color.purple}
	${(props) => props.running && ActiveTimer};
`;
export const NormalBlueText = styled(BoldBlueText)`
	font-size: 16px;
	 ${Color.darkBlue};
`;
export const LightBlueText = styled(BoldBlueText)`
	font-weight: 400;
`;
export const SlightlyBoldBlueText = styled(BoldBlueText)`
`;

export const WelcomeMessageDashboard = ({ username, on }) => (
	<>
		<WelcomeMessageWrapper>
			<WelcomeMessageText>
                Welcome
				{' '}
				{username}
			</WelcomeMessageText>
			<WelcomeMessageText>
                Today is
				{' '}
				{getCurrentDay()}
			</WelcomeMessageText>
			<NormalPurpleText running={on}>
				Your time tracker is
				{' '}
				{on ? 'on' : 'off'}
			</NormalPurpleText>

		</WelcomeMessageWrapper>
	</>
);
