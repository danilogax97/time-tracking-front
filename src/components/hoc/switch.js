import { connect } from 'react-redux';
import React, { Component } from 'react';

const withSwitch = (WrappedComponent) => {
	class Wrapper extends Component {
		render() {
			const {
				isLoggedIn,
			} = this.props;

			if (isLoggedIn) {
			    // maybe pass some props here
				return <WrappedComponent.LoggedIn {...this.props} />;
			}
			// and pass different props here
			return <WrappedComponent.LoggedOut {...this.props} />;
		}
	}
	const mapStateToProps = (state) => ({
		isLoggedIn: state.getIn(['signInReducer', 'user', 'loggedIn']),
	});
	return connect(mapStateToProps, null)(Wrapper);
};
export default withSwitch;
