import React, { Component } from 'react';
import { Redirect } from 'react-router';
import { connect } from 'react-redux';


const Unprotected = (WrappedComponent) => {
	class Wrapper extends Component {
		render() {
			const {
				isLoggedIn,
			} = this.props;

			if (isLoggedIn) {
				return <Redirect to="/" />;
			}
			return <WrappedComponent {...this.props} />;
		}
	}
	const mapStateToProps = (state) => ({
		isLoggedIn: state.getIn(['signInReducer', 'user', 'loggedIn']),
	});
	return connect(mapStateToProps, null)(Wrapper);
};
export default Unprotected;
