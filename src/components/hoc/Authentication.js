import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';

const Authenticaiton = (WrappedComponent) => {
	class Wrapper extends PureComponent {
		render() {
			const {
				isLoggedIn,
			} = this.props;

			if (!isLoggedIn) {
				return <Redirect to="/sign-in" />;
			}
			return <WrappedComponent {...this.props} />;
		}
	}
	const mapStateToProps = (state) => ({
		isLoggedIn: state.getIn(['signInReducer', 'user', 'loggedIn']),
	});
	return connect(mapStateToProps, null)(Wrapper);
};


export default Authenticaiton;
