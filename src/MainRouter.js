import { Route, Switch } from 'react-router';
import React from 'react';
import { ConnectedRouter } from 'connected-react-router/immutable';
import Welcome from './containers/welcome';
import SignIn from './containers/sign-in';
import SignUp from './containers/sign-up';
import Reports from './containers/reports';
import Dashboard from './containers/dashboard';
import Authentication from './components/hoc/Authentication';
import SignOut from './containers/sign-out';
import NotFound from './containers/notfound';
import ErrorBoundary from './components/ErrorBoundary';

const MainRouter = ({ history }) => (
	<ConnectedRouter history={history}>
		<ErrorBoundary>
			<Switch>
				<Route
					exact
					path="/"
					component={Welcome}
				/>
				<Route
					path="/sign-in"
					component={SignIn}
				/>
				<Route
					path="/sign-up"
					component={SignUp}
				/>
				<Route
					path="/reports"
					component={Authentication(Reports)}

				/>
				<Route
					path="/dashboard"
					component={Authentication(Dashboard)}

				/>
				<Route
					path="/sign-out"
					component={SignOut}
				/>
				<Route
					component={NotFound}
				/>
			</Switch>
		</ErrorBoundary>
	</ConnectedRouter>
);

export default MainRouter;
