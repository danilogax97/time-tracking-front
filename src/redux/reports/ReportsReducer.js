import {
	fromJS,
} from 'immutable';
import Types from './ReportsTypes';

const initialState = fromJS({
	reports: {},
	error: undefined,
	loading: false,
});

export default (state = initialState, action) => {
	switch (action.type) {
	case Types.FETCH_REPORTS_SUCCESS:
		return state.set('reports', fromJS(action.reports)).set('loading', false); //treba li da pretvoirim action.reports u immutable objekat?
	case Types.FETCH_REPORTS:
		return state.set('loading', true);
	case Types.FETCH_REPORTS_FAIL:
		return state.set('loading', false).set('error', action.error);
	default: return state;
	}
};
