import {
	fromJS,
} from 'immutable';
import Types from './SignInTypes';

const initialState = fromJS({
	loading: false,
	error: undefined,
	user: {
		loggedIn: false,
		token: undefined,
		email: undefined,
		username: undefined,
	},
});

export default function signInReducer(state = initialState, action) {
	switch (action.type) {
	case Types.LOGIN:
		return state.set('loading', true);
	case Types.LOGIN_SUCCESS: return state.set('loading', false).merge({
		user: {
			loggedIn: true,
			token: action.token,
			email: action.email,
			username: action.username,
		},
	});
	case Types.LOGIN_FAIL: return state.set('loading', false)
		.set('error', action.error);
	//KAKO OVO BOLJE DA NAPISEM
	case 'ADD_TOKEN_FROM_PERSISTANCE':
		return state.setIn(['user', 'token'], action.token)
			.setIn(['user', 'loggedIn'], true)
			.setIn(['user', 'username'], action.username);
	case 'CLEAR_USER':
		return state.set('user', undefined);
	default: return state;
	}
}
