import { replace } from 'connected-react-router';
import Api from '../../lib/api';
import Types from './SignInTypes';

export function fetch() {
	return {
		type: Types.LOGIN,
	};
}
export function fetchSuccess(token, email, username) {
	localStorage.setItem('TTtoken', token);
	localStorage.setItem('TTusername', username);
	return {
		type: Types.LOGIN_SUCCESS,
		token,
		email,
		username,
	};
}
export function fetchFail(error) {
	return {
		type: Types.LOGIN_FAIL,
		error,
	};
}
export function fetchSignIn(data) {
	return async (dispatch) => {
		dispatch(fetch());
		try {
			const response = await Api.signIn(data);
			dispatch(fetchSuccess(response.data.accessToken.accessToken, response.data.user.email, response.data.user.fullName));
			dispatch(replace('/dashboard'));
		} catch (err) {
			dispatch(fetchFail(err));
		}
	};
}
export function signOut() {
	return (dispatch) => {
		localStorage.removeItem('TTtoken');
		localStorage.removeItem('TTusername');
		dispatch({ type: Types.SIGN_OUT });
	};
}
