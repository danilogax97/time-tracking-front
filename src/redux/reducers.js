// reducers.js
import { combineReducers } from 'redux-immutable';
import { connectRouter } from 'connected-react-router/immutable';
import signInReducer from './sign-in/SignInReducer';
import timerReducer from './timer/TimerReducer';
import reportsReducer from './reports/ReportsReducer';
import errorReducer from './error/ErrorReducer';

const createRootReducer = (history) => combineReducers({
	router: connectRouter(history),
	signInReducer,
	timerReducer,
	reportsReducer,
	errorReducer,
});
export default createRootReducer;
