import Types from './ErrorTypes';

export function setError(error) {
	return (dispatch) => dispatch({
		type: Types.SET_ERROR,
		error,
	});
}
