import {
	fromJS,
} from 'immutable';
import Types from './ErrorTypes';

const initialState = fromJS({
	error: undefined,
});

export default (state = initialState, action) => {
	switch (action.type) {
	case Types.SET_ERROR:
		return state.set('error', action.error);
	default: return state;
	}
};
