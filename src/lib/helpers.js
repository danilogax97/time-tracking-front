import dayjs from 'dayjs';

export const figureOutIfItsTodayOrYesterday = (date) => {
	let now = dayjs();
	const today = { day: now.get('date'), month: now.get('month'), year: now.get('year') };
	now = now.subtract(1, 'day');
	const yesterday = { day: now.get('date'), month: now.get('month'), year: now.get('year') };

	if (date.get('date') === today.day && date.get('month') === today.month && date.get('year') === today.year) {
		return 'Today';
	}
	if (date.get('date') === yesterday.day && date.get('month') === yesterday.month && date.get('year') === yesterday.year) {
		return 'Yesterday';
	}
	return null;
};
export const displayDifference = (startTime, endTime) => (
	dayjs(endTime).subtract(startTime.hour(), 'hour').subtract(startTime.minute(), 'minute').subtract(startTime.second(), 'second')
		.format('HH:mm:ss')
);
